<?php namespace Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes, Traits\Sortable, Traits\Act;

    protected $table = 'company';
    protected $visible = array(
        'id',
        'name',
        'parent',
        'site_id',
        'params',
        'description',
        'content'
    );

    public function getParamsAttribute($value)
    {
        return json_decode($value);
    }

    public function setParamsAttribute($value)
    {
        $this->attributes['params'] = json_encode($value);
    }
}
