<?php namespace Model;

class Item extends Model
{
    use Traits\Image, Traits\Company;

    protected $table = 'item';
    protected $visible = array(
        'id',
        'name',
        'company_id',
        'cat_id',
        'site_id',
        'price',
        'params',
        'description',
        'content'
    );

    public function setPriceAttribute($value)
    {
        return $this->attributes['price'] = json_encode($value);
    }

    public function setParamsAttribute($value)
    {
        return $this->attributes['params'] = json_encode($value);
    }

    public function getPriceAttribute($value)
    {
        return json_decode($value);
    }

    public function getParamsAttribute($value)
    {
        return json_decode($value);
    }

    public function getParamsColorAttribute()
    {
        return $this->params ? $this->params->color : null;
    }

    public static function getValue($key, $item, $default = '')
    {
        if (is_array($item)) {
            $path = explode('->', $key);
            foreach ($path as $key) {
                if (isset($item[$key])) {
                    $item = is_object($item[$key]) ? (array) $item[$key] : $item[$key];
                } else {
                    return $default;
                }
            }
            return $item;
        }
        return $item;
    }

    public function getParamsValue($key, $default = '')
    {
        $item = (array) $this->params;
        return static::getValue($key, $item, $default);
    }

    public function getParamsSizeAttribute()
    {
        return $this->params ? $this->params->size : null;
    }

    public function getParamsAgeAttribute()
    {
        return $this->params ? $this->params->age : null;
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'params_color');
    }

    public function age()
    {
        return $this->hasOne(Age::class, 'id', 'params_age');
    }

    protected function createDescription()
    {
        $part = [];
        $name = $this->name;
        if ($this->company && $this->company->name) {
            $name .= ' (' . $this->company->name . ')';
        }

        $part[] = $name . '.';
        if ($this->price && $this->price->price1) {
            $part[] = 'Цена проката: ' . $this->price->price1 . '₽;';
        }

        if ($this->age && $this->age->label) {
            $part[] = 'эпоха: ' . $this->age->label . ';';
        }

        if ($this->color && $this->color->label) {
            $part[] = 'цвет: ' . $this->color->label . ';';
        }

        if ($this->getParamsValue('size') && $this->getParamsValue('size->from')) {
            $size = 'размер: ' . $this->getParamsValue('size->from');
            if ($this->getParamsValue('size->to')) {
                $size .= '-' . $this->getParamsValue('size->to');
            }
            $part[] = $size;
        }

        if ($this->company && $this->company->city && $this->company->city->name) {
            $part[] = 'город: ' . $this->company->city->name . ';';
        }

        $this->description = implode(' ', $part);
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(function($item) {
            $item->createDescription();
        });
    }
}
