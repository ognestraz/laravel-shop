<?php namespace Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Catalog extends Model
{
    use SoftDeletes, Traits\Tree, Traits\Act;

    protected $table = 'catalog';
    protected $visible = array(
        'id',
        'name',
        'parent',
        'site_id',
        'description',
        'content'
    );
}
