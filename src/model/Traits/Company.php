<?php namespace Model\Traits;

trait Company
{
    public function company()
    {
        return $this->belongsTo(config('model.company') ?: 'Model\Company');
    }
}
