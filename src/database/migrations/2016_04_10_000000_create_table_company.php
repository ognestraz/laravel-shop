<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('act');
            $table->integer('city_id');
            $table->integer('user_id');
            $table->integer('cat_id');
            $table->integer('site_id');
            $table->string('name');
            $table->string('description');
            $table->text('content');
            $table->json('params');
            $table->string('source');
            $table->integer('sort');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company');
    }
}
