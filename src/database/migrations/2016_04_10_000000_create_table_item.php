<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('act');
            $table->boolean('main');
            $table->integer('company_id');
            $table->integer('cat_id');
            $table->integer('site_id');
            $table->string('article');
            $table->string('name');
            $table->string('description');
            $table->text('content');
            $table->json('price');
            $table->json('params');
            $table->boolean('import');
            $table->integer('import_id');
            $table->string('source');
            $table->integer('sort');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item');
    }
}
