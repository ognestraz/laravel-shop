<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Model\Catalog;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseModel::unguard();

        $list = [
            ['id' => 1, 'parent' => 0, 'name' => 'Ring'],
            ['id' => 2, 'parent' => 0, 'name' => 'Chain'],
            ['id' => 3, 'parent' => 1, 'name' => 'Golden Ring'],
            ['id' => 4, 'parent' => 1, 'name' => 'Silver Ring'],
            ['id' => 5, 'parent' => 2, 'name' => 'Golden Chain'],
            ['id' => 6, 'parent' => 2, 'name' => 'Silver Chain'],
        ];

        $dataBase = [
            'act' => true,
            'main' => 0,
            'import' => false,
            'import_id' => 0,
            'source' => '',
            'description' => '',
            'content' => '',
            'sort' => 0
        ];

        DB::table((new Catalog())->getTable())->truncate();

        foreach ($list as $row) {
            Catalog::create(array_merge($dataBase, $row));
        }
    }
}
