<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Model\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseModel::unguard();

        $list = [
            ['id' => 1, 'name' => 'Rubin'],
            ['id' => 2, 'name' => 'Topaz'],
            ['id' => 3, 'name' => 'Aquamarin'],
            ['id' => 4, 'name' => 'Ismerald'],
            ['id' => 5, 'name' => 'Ruby']
        ];

        $dataBase = [
            'act' => true,
            'source' => '',
            'description' => '',
            'content' => '',
            'sort' => 0
        ];

        DB::table((new Company())->getTable())->truncate();

        foreach ($list as $row) {
            Company::create(array_merge($dataBase, $row));
        }
    }
}
