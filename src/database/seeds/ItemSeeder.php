<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Model\Item;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseModel::unguard();

        $list = [
            ['id' => 1, 'cat_id' => 3, 'name' => 'Ring Gold 585', 'article' => 'r.0001'],
            ['id' => 2, 'cat_id' => 3, 'name' => 'Ring Gold 725', 'article' => 'r.0002'],
            ['id' => 3, 'cat_id' => 4, 'name' => 'Ring Silver', 'article' => 'r.0003'],
            ['id' => 4, 'cat_id' => 6, 'name' => 'Chain Silver', 'article' => 'c.0001'],
            ['id' => 5, 'cat_id' => 6, 'name' => 'Chain Silver 925', 'article' => 'c.0002'],
            ['id' => 6, 'cat_id' => 5, 'name' => 'Chain Gold', 'article' => 'c.0003'],
        ];

        $dataBase = [
            'act' => true,
            'main' => 0,
            'import' => false,
            'import_id' => 0,
            'source' => '',
            'description' => '',
            'content' => '',
            'sort' => 0
        ];

        DB::table((new Item())->getTable())->truncate();

        foreach ($list as $row) {
            Item::create(array_merge($dataBase, $row));
        }
    }
}
